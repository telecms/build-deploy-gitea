FROM debian:bullseye
RUN apt update
# build dep
RUN apt install -y make librsvg2-bin imagemagick git wget nodejs npm
# deploy dep
RUN apt install -y openssh-client rsync
# entrypoint (use TARGET_UID to create a user with /root as a home directory, but a different UID, and use that UID to run the supplied command)
RUN apt install -y sudo && \
    printf '#!/bin/sh\nif [ -z "$*" ]; then set -- /bin/bash; fi\nif [ -n "$TARGET_UID" ]; then getent passwd "u$TARGET_UID" >/dev/null || useradd -M -d /root -u "$TARGET_UID" "u$TARGET_UID" -s /bin/sh && chmod go+rwx /root && exec sudo -u "u$TARGET_UID" "$@"; else exec "$@"; fi\n' > /sbin/container-init && \
    chmod +x /sbin/container-init
ENTRYPOINT ["/sbin/container-init"]
